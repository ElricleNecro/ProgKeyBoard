ProgKeyBoard
------------
You will need:
```
luarocks install evdev
luarocks install lyaml
```

To run it, just Modify the name of the device you want to control inside `run_keyboard.sh`. Then, run:
```
/usr/bin/su -p
./run_keyboard.sh
```
