#!/usr/bin/env bash

KEYBOARD='HOLTEK USB Keyboard'
id=$(xinput --list | grep "${KEYBOARD}" | cut -f2 | cut -d= -f2 | sort | head -n1 )
device=$(xinput --list-props ${id} | grep 'Device Node' | cut -f3 | sed 's/"//g')

xinput --set-prop ${id} "Device Enabled" 0

echo ${id} ${device}

lua keyboard.lua ${device}
