local evdev = require "evdev"
local cte   = require 'evdev.constants'
local lyaml  = require 'lyaml'
-- local Uinput = require 'Uinput'

local path  = ...

local codes = {}

do
	local cte   = require 'evdev.constants'
	for k, v in pairs(cte) do
		codes[v] = tostring(k)
		print(v, k)
	end
end

local Uinput = { _mt = {} }

function Uinput:useEvent(...)
	self.ui:useEvent(...)
end

function Uinput:useKey(...)
	self.ui:useEvent(...)
end

function Uinput:init(...)
	self.ui:init(...)
end

function Uinput:write(...)
	self.ui:write(...)
end

function Uinput.syn(self)
	self:write(evdev.EV_SYN, evdev.SYN_REPORT, 0)
end

function setLock(device, key, action)
	action = action or 'off'

	if action == 'off' then
		device:set_led(key, 0)
	elseif action == 'on' then
		device:set_led(key, 1)
	end
end

function setNumLock(device, action)
	setLock(device, evdev.LED_NUML, action)
end

function setCapsLock(device, action)
	setLock(device, evdev.LED_CAPSL, action)
end

function resetLeds(device)
	setNumLock(device, 'off')
	setCapsLock(device, 'off')
end

function startRecording(dev)
	print("Starting recording macro")
	setNumLock(dev, 'on')

	return true
end

function stopRecording(dev, recording_buffer)
	print("Stop recording macro")
	print("Buffer:", recording_buffer)

	setNumLock(dev, 'off')
	setCapsLock(dev, 'on')

	print("Hit a key to save the buffer.")

	return false
end

function saveMacro(key, recording_buffer, macros)
	macros[key] = { key_down = recording_buffer }

	print(macros)

	writeConfig(macros)
end

function readConfig(cfg)
	cfg = cfg or "test.yaml"

	f = io.open(cfg, 'r')
	config = lyaml.load( f:read("*all") )
	f:close()

	return config
end

function writeConfig(config, cfg)
	cfg = cfg or "test.yaml"

	f = io.open(cfg, 'w')
	f:write(
		lyaml.dump({ config })
	)
	f:close()
end

function emit(ui, key)
	ui:write(evdev.EV_KEY, key, 1)
	ui:write(evdev.EV_KEY, key, 0)
	Uinput.syn(ui)
end

function readKeyBoard(device, ui)
	config = readConfig()
	activated = false

	while true do
		local timestamp, eventType, eventCode, value = device:read()
		if eventType == evdev.EV_KEY then
			if config[ codes[eventCode] ] and value == 1 then
				activated = true
			elseif config[ codes[eventCode] ] and value == 0 then
				activated = false
			elseif eventCode == evdev.KEY_BACKSPACE then
				break
			end
		end

		if activated and config[ codes[eventCode] ] then
			buf = config[ codes[eventCode] ]
			for idx = 1, #buf do
				emit(ui, cte[ buf[ idx ] ])
			end
		end
	end
end

local dev = evdev.Device(path, true)
dev:grab(true)
local ui = evdev.Uinput('/dev/uinput')
-- local ui = Uinput:new('/dev/uinput')
ui:useEvent(evdev.EV_KEY)

do
	local cte   = require 'evdev.constants'
	for _, v in pairs(cte) do
		ui:useKey(v)
	end
end

ui:init()
readKeyBoard(dev, ui)
